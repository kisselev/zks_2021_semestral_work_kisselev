package cz.cvut.fel.omo.foodChain.product;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.states.FinalPackedState;
import cz.cvut.fel.omo.foodChain.states.State;
import cz.cvut.fel.omo.foodChain.states.TemporaryPackedState;
import cz.cvut.fel.omo.foodChain.states.UnpackedState;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.Main.logger;

public class FoodEntity {

    private FoodEnum name;
    private int quantity;
    private final int storageTemperature;
    private final int calories;
    private final String category;
    private List<String> doneActions = new ArrayList<>();
    private State state;

    public FoodEntity(FoodEnum name, String category, Integer quantity, Integer storageTemperature, Integer calories) {
        this.name = name;

        if(category == null)
            throw new IllegalArgumentException("The category of a food product should not be null");
        this.category = category;

        if(quantity == null)
            throw new IllegalArgumentException("The quantity of a food product should not be null");
        if(quantity < 0)
            throw new IllegalArgumentException("The quantity should be a positive number");
        if (quantity > 50)
            throw new IllegalArgumentException("The maximum quantity is 50 elements");
        this.quantity = quantity;

        if (storageTemperature < -20)
            throw new IllegalArgumentException("The provided freezing temperature is too cold");
        if(storageTemperature > 20)
            throw new IllegalArgumentException("Provided storage temperature is too high");
        this.storageTemperature = storageTemperature;


        if(calories == null) {
            throw new IllegalArgumentException("Calories count should be provided for a product");
        }
        if(calories < 0) {
            throw new IllegalArgumentException("Calories count should not be a negative number");
        }
        if(calories > 2000) {
            throw new IllegalArgumentException("Calories count cannot be higher than 2000");
        }
        this.calories = calories;
        this.state = new UnpackedState(this);
    }


    public FoodEntity(FoodEnum name, Integer quantity, Integer storageTemperature, Integer calories) {
        if (name == null)
            throw new IllegalArgumentException("The name of a food product should not be null");
        this.name = name;

        if(quantity == null)
            throw new IllegalArgumentException("The quantity of a food product should not be null");
        if(quantity < 0)
            throw new IllegalArgumentException("The quantity should be a positive number");
        if (quantity > 50)
            throw new IllegalArgumentException("The maximum quantity is 50 elements");
        this.quantity = quantity;

        if (storageTemperature == null)
            throw new IllegalArgumentException("The storage temperature should not be null");
        if (storageTemperature < -20)
            throw new IllegalArgumentException("The provided freezing temperature is too cold");
        if(storageTemperature > 20)
            throw new IllegalArgumentException("Provided storage temperature is too high");
        this.storageTemperature = storageTemperature;


        if(calories == null) {
            throw new IllegalArgumentException("Calories count should be provided for a product");
        }
        if(calories < 0) {
            throw new IllegalArgumentException("Calories count should not be a negative number");
        }
        if(calories > 2000) {
            throw new IllegalArgumentException("Calories count cannot be higher than 2000");
        }
        this.calories = calories;
        this.category= "blank";
        this.state = new UnpackedState(this);
    }


    public FoodEnum getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getStorageTemperature() {
        return storageTemperature;
    }

    public int getCalories() {
        return calories;
    }

    public String getCategory() {
        return category;
    }

    public List<String> getDoneActions() {
        return doneActions;
    }

    public void addAction(String action) {
        doneActions.add(action);
    }

    /**
     * uses for product report
     */
    public void writeAllActions() {
        for (String action : doneActions) {
            System.out.println(action);
        }
    }

    public void writeAllActionsText(String fileName) {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(fileName), "utf-8"))) {
            writer.write("-------------------- FOODENTITY INFORMER REPORT --------------------\n");
            for (String action : doneActions) {
                writer.write(action + "\n");
            }
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }

    /**
     * uses for change food entity state to unpacked
     */
    public void useForCooking() {
        if (!state.cook()) state = new UnpackedState(this);
    }

    /**
     * if applicant is customer food entity will be packed in final packing,otherwise in temporary packing
     *
     * @param apllicant
     */
    public void transport(Party apllicant) {
        if (apllicant instanceof Customer) {
            if (!state.presentProductToCustomer()) state = new FinalPackedState(this);
        } else if (!state.transport()) state = new TemporaryPackedState(this);
    }

    public State getState() {
        return state;
    }

    public String getStringState() {
        return state.getStringState();
    }

}
