package cz.cvut.fel.omo.foodChain.operations;

public enum OperationEnum {
    GROW,
    PRODUCE,
    STORE,
    SELL,
    RETAIL,
    CUSTOMER
}
