package cz.cvut.fel.omo.foodChain.strategy;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;

import java.util.HashMap;
import java.util.Set;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.Main.logger;



public class CookIceCream implements ManufacturerStrategy {

    private Manufacturer manufacturer;
    private HashMap<FoodEnum, FoodEntity> receipt = new HashMap<>() {{
        put(MILK, null);
        put(SUGAR, null);
    }};

    public CookIceCream(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public HashMap<FoodEnum, FoodEntity> getReceipt() {
        return receipt;
    }

    public boolean isReadyForCooking() {
        for (FoodEnum receiptItem : receipt.keySet()) {
            if (receipt.get(receiptItem) == null) {
                return false;
            }
        }
        return true;
    }

    public void checkIfNeedSomethingForCooking() {
        Set<FoodEnum> receipt = this.receipt.keySet();

        for (FoodEnum receiptItem : receipt) {
            boolean sendReq = true;
            for (FoodEntity foodEntity : manufacturer.getProducts()) {
                if (foodEntity.getName() == receiptItem) {
                    sendReq = false;
                    break;
                }
            }
            if (sendReq) manufacturer.sendRequest(receiptItem, 1);
        }
    }

    public void fillReceipt(){
        for(FoodEnum ingredient : receipt.keySet()){
            for(FoodEntity manufacturerFood : manufacturer.getProducts()){
                if(manufacturerFood.getName() == ingredient){
                    receipt.replace(ingredient, manufacturerFood);
                }
            }
        }
    }

    /**
     * cooking ice cream from ingredients
     *
     * @return ready product
     */
    public FoodEntity cook() {

        Set<FoodEnum> receipt = this.receipt.keySet();

        fillReceipt();

        if (isReadyForCooking()) {
            for (FoodEnum receiptItem : receipt) {
                for (int i = 0; i < manufacturer.getProducts().size(); i++) {
                    FoodEnum productName = manufacturer.getProducts().get(i).getName();
                    if (productName.equals(receiptItem) && this.receipt.get(receiptItem) == null) {
                        this.receipt.replace(receiptItem, manufacturer.getProducts().get(i));
                        manufacturer.getProducts().get(i).addAction(manufacturer.getProducts().get(i) + " with name " +
                                manufacturer.getProducts().get(i).getName() + " was used for cooking meal by " + this);
                        manufacturer.getProducts().get(i).useForCooking();
                        manufacturer.getProducts().remove(i);
                    }
                }
            }

            for (FoodEnum receiptItem : receipt) {
                this.receipt.replace(receiptItem, null);
            }
            FoodEntity meal = new FoodEntity(ICECREAM, "coldDessert", 1, -10, 400);
            meal.addAction(meal + " with name " + ICECREAM.toString() + " was created by " + this);
            return meal;
        }
        logger.warning("PRODUCT ICE CREAM WAS NOT CREATED BY " + manufacturer.getName());
        return null;
    }
}

