package cz.cvut.fel.omo.foodChain.operations;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;

public class Request {

    private Party applicant;
    private FoodEnum foodEnum;
    private int quantity;
    private OperationEnum operationType;
    private int price;
    public boolean done = false;

    public Request(Party applicant, FoodEnum foodEnum, Integer quantity, OperationEnum operationType) {
        if (applicant == null)
            throw new IllegalArgumentException("Applicant shouldn't be null.");
        this.applicant = applicant;

        if (foodEnum == null)
            throw new IllegalArgumentException("FoodEnum shouldn't be null.");
        this.foodEnum = foodEnum;

        if (quantity == null)
            throw new IllegalArgumentException("Quantity shouldn't be null.");
        if (quantity < 0)
            throw new IllegalArgumentException("Quantity shouldn't be a negative number.");
        if (quantity > 10)
            throw new IllegalArgumentException("One request can't have more than 10 elements.");
        this.quantity = quantity;

        if (operationType == null)
            throw new IllegalArgumentException("Quantity shouldn't be null.");
        this.operationType = operationType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Party getApplicant() {
        return applicant;
    }

    public FoodEnum getFoodEnum() {
        return foodEnum;
    }

    public int getQuantity() {
        return quantity;
    }

    public OperationEnum getOperationType() {
        return operationType;
    }


    @Override
    public String toString() {
        return "REQUEST: " + "applicant = " + applicant +
                ", foodEnum = " + foodEnum + ", quantity = " + quantity +
                ", operationType = " + operationType + '.';
    }
}
