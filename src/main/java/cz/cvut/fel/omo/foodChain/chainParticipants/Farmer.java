package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.operations.*;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.*;

/**
 * The class describes the general state and behavior of a farmer.
 *
 * @author Artem Hurbych, Pavel Paklonski
 */
public abstract class Farmer extends ChainParticipant {

    private String country;
    private String city="";
    private Integer age=0;
    private List<FoodEnum> ingredientList;
    private List<FoodEntity> products = new ArrayList<>();
    private OperationEnum operationType = GROW;

    public Farmer(String name, String country, int cashAccount, List<FoodEnum> ingredientList, TransactionInformer transactionInformer) {
        super(name, cashAccount, transactionInformer);

        if (country == null)
            throw new IllegalArgumentException("Name of a country shouldn't be null.");
        if (country.equals(""))
            throw new IllegalArgumentException("Name of a country shouldn't be null.");
        if(name.length() < 4)
            throw new IllegalArgumentException("Provided name of a country is shorter than the shortest possible name of any country in the world.");
        if(name.length() > 56)
            throw new IllegalArgumentException("Provided name of a country is larger than the shortest largest name of any country in the world.");
        this.country = country;


        this.ingredientList = ingredientList;
    }


    public Farmer(String name, String country, String city, Integer age, Integer cashAccount, List<FoodEnum> ingredientList, TransactionInformer transactionInformer) {
        super(name, cashAccount, transactionInformer);

        if (age == null)
            throw new IllegalArgumentException("Age shouldn't be null.");
        if (age < 17)
            throw new IllegalArgumentException("Age can't be negative");
        if (age > 130)
            throw new IllegalArgumentException("Age can't be more than 130");
        this.age = age;

        if (city == null)
            throw new IllegalArgumentException("Name of a city shouldn't be null.");
        if (city.equals(""))
            throw new IllegalArgumentException("Name of a city shouldn't be null.");
        if(city.length() < 2)
            throw new IllegalArgumentException("Provided name of a city is shorter than the shortest possible name of any city in the world.");
        if(city.length() > 85)
            throw new IllegalArgumentException("Provided name of a city is larger than the shortest largest name of any city in the world.");
        this.city = city;


        if (country == null)
            throw new IllegalArgumentException("Name of a country shouldn't be null.");
        if (country.equals(""))
            throw new IllegalArgumentException("Name of a country shouldn't be null.");
        if(country.length() < 4)
            throw new IllegalArgumentException("Provided name of a country is shorter than the shortest possible name of any country in the world.");
        if(country.length() > 56)
            throw new IllegalArgumentException("Provided name of a country is larger than the shortest largest name of any country in the world.");
        this.country = country;

        if (ingredientList == null)
            throw new IllegalArgumentException("Ingredient list shouldn't be null.");
        this.ingredientList = ingredientList;
    }

    public OperationEnum getOperationType() {
        return operationType;
    }

    /**
     * Check if the farmer can execute the request.
     *
     * @param request the instance of Request
     * @return true - if the farmer can execute the request
     */
    public boolean isAgreeToExecute(Request request) {
        return ingredientList.contains(request.getFoodEnum());
    }

    public abstract FoodEntity process(Request request);

    /**
     * Add the entity of food to the list of products.
     *
     * @param foodEntity the instance of food that was produced by someone
     */
    public void addFood(FoodEntity foodEntity) {
        products.add(foodEntity);
    }

}
