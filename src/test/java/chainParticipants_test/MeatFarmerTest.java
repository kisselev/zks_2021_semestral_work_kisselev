package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;


import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import static org.junit.Assert.*;

public class MeatFarmerTest {
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    MeatFarmer meatFarmer;
    private PriceWriter visitor;
    private TransactionInformer transactionInformer;

    private int getQuantity(List<FoodEntity> entities) {
        int sum = 0;
        for (FoodEntity entity: entities){
            sum += entity.getQuantity();
        }
        return sum;
    }

    @Before
    public void prepare() {
        visitor = new PriceWriter();
        transactionInformer = new TransactionInformer();
        meatFarmer = new MeatFarmer("Test Meat Farmer", "Test Country", 1000, meatIngredients, transactionInformer);
    }

    /**
     * This test assures the requests are processed right by the meat farmer.
     */
    @Test
    @Tag("Unittest")
    public void processTest() {
        Request request = new Request(meatFarmer, MEAT, 1, meatFarmer.getOperationType());
        FoodEntity request_result = meatFarmer.process(request);
        assertEquals(MEAT, request_result.getName());
        assertEquals(1, request_result.getQuantity());
    }

    /**
     * This test checks registering to all suitable channels for this type of ChainParticipant
     */
    @Test
    @Tag("Unittest")
    public void registerToChannelTest() {
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, meatFarmer);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        meatFarmer.registerToTheChannel();

        assertEquals(MeatChannel.getMeatChannel().getCurrentParticipants(), participants);
        assertEquals(PaymentChannel.getPaymentChannel().getCurrentParticipants(), participants);
    }

    /**
     * This test accepts a visitor and checks whether it is working by assuring the prices are set correctly.
     */
    @Test
    @Tag("Unittest")
    public void acceptTest() {
        meatFarmer.accept(visitor);

        ArrayList<Integer> setPrices = new ArrayList<>();
        for (FoodEnum food_element: meatIngredients) {
            setPrices.add(meatFarmer.askPrice(food_element, 1));
        }
        List<Integer> list = Arrays.asList(20, 50, 25, 30);
        assertEquals(list, setPrices);
    }


    /**
     * This test processes several request at once, checking the application for correctness dealing with multiple data
     * inputs.
     */
    @Test
    @Tag("Unittest")
    public void processSeveralRequestsTest() {
        Request request1 = new Request(meatFarmer, MILK, 5, meatFarmer.getOperationType());
        Request request2 = new Request(meatFarmer, BUTTER, 5, meatFarmer.getOperationType());
        Request request3 = new Request(meatFarmer, MEAT, 5, meatFarmer.getOperationType());

        FoodEntity request_result1 = meatFarmer.process(request1);
        FoodEntity request_result2 = meatFarmer.process(request2);
        FoodEntity request_result3 = meatFarmer.process(request3);

        List<FoodEntity> entities = Arrays.asList(request_result1, request_result2, request_result3);
        assertEquals(MILK, request_result1.getName());
        assertEquals(15, getQuantity(entities));
    }
}
