package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;


import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;

public class ManufacturerTest {
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    Manufacturer manufacturer;
    Customer customer1;

    @Before
    public void prepare() {
        manufacturer = new Manufacturer("Testing Retailer",1000, DRANIKI, transactionInformer);
        customer1 = new Customer("Test Customer1", 400, transactionInformer);

    }

    /**
     * This test checks registering to all suitable channels for this type of ChainParticipant
     */
    @Test
    @Tag("Unittest")
    public void registerToTheChannelTest() {
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, manufacturer);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        manufacturer.registerToTheChannel();

        Assertions.assertTrue(VegetableChannel.getVegetableChannel().getCurrentParticipants().contains(manufacturer));
        Assertions.assertTrue(MeatChannel.getMeatChannel().getCurrentParticipants().contains(manufacturer));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().getCurrentParticipants().contains(manufacturer));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().getCurrentParticipants().contains(manufacturer));
    }

    /**
     * This test tests whether the food is added and the information on the transfer is updated.
     */
    @Test
    @Tag("Unittest")
    public void addFood() {
        FoodEntity food = new FoodEntity(DRANIKI, "readyMeal", 8, 4,200);
        manufacturer.addFood(food);
        Assertions.assertTrue(manufacturer.getProducts().contains(food));
        Assertions.assertTrue(food.getDoneActions().contains(food + " with name " + food.getName() + " come to " + manufacturer));
    }

    /**
     * This test assures the requests are processed right by the manufacturer.
     */
    @Test
    @Tag("Unittest")
    public void processTest() {
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, manufacturer, customer1);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        manufacturer.registerToTheChannel();
        customer1.registerToTheChannel();

        manufacturer.addFood(new FoodEntity(POTATO,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(ONION,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(FLOUR,"vegetable", 1, 15, 400));
        manufacturer.addFood(new FoodEntity(EGGS,"meat", 1, 4, 150));
        Request request = new Request(customer1, DRANIKI, 1, manufacturer.getOperationType());
        FoodEntity outputFoodEntity = manufacturer.process(request);
        Assertions.assertEquals(1, outputFoodEntity.getQuantity());
    }


    /**
     * This test should not find any BORSHCH food entity and fail
     * */
    @Test
    @Tag("Unittest")
    public void processShouldFailTest() {
        FoodEntity entity = new FoodEntity(ICECREAM, "coldDessert", 1, -20,20);
        manufacturer.addFood(entity);
        Request request = new Request(manufacturer, BORSHCH, 1, manufacturer.getOperationType());
        FoodEntity outputFoodEntity = manufacturer.process(request);
        Assertions.assertNull(outputFoodEntity);
    }

}
