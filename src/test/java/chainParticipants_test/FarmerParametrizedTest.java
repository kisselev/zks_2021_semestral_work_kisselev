package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.Farmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import input_converters.NumberConverter;
import input_converters.StringConverter;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.ICECREAM;

public class FarmerParametrizedTest {
    List<FoodEnum> vegetableIngredients;

    @Before
    public void prepare() {
        vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    }

    /**
     * This test assures the constructor of a Farmer class is correct, analyzing its parameters
     * The method for creating combinations is 3 way method.
     * @param country: the country location
     * @param age: age of this particular farmer
     * @param city: city he/she is located
     * @param ingredientsList: ingredients the farmer uses
     * @param expected_outcome: expected outcome of a test
     */
    @ParameterizedTest
    @CsvFileSource(resources = "/Farmer3way.csv", delimiter = ',', numLinesToSkip = 1)
    @Tag("Parameterized")
    public void FarmerCreation3way(
            String country,
            @ConvertWith(NumberConverter.class) Integer age,
            String city,
            String ingredientsList,
            @ConvertWith(NumberConverter.class) Integer expected_outcome
    ) {
        int actual_outcome = 1;
        List<FoodEnum> inputIngredientsList;

        if (country.equals("None")) country = null;
        else if (country.equals("-")) country = "";

        if (city.equals("None")) city = null;
        else if (city.equals("-")) city = "";

        if (ingredientsList.equals("None")) inputIngredientsList = null;
        else inputIngredientsList = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);

        try {
            new VegetableFarmer("test", country, city, age,10, inputIngredientsList, null);
        } catch (IllegalArgumentException e) {
            actual_outcome = 0;
        }
        Assertions.assertEquals(expected_outcome, actual_outcome);
    }


    /**
     * This test assures the constructor of a Farmer class is correct, analyzing its parameters
     * The method for creating combinations is pairwise testing or 2 way method.
     * @param country: the country location
     * @param age: age of this particular farmer
     * @param city: city he/she is located
     * @param ingredientsList: ingredients the farmer uses
     * @param expected_outcome: expected outcome of a test
     */
    @ParameterizedTest
    @CsvFileSource(resources = "/Farmer2way.csv", delimiter = ',', numLinesToSkip = 1)
    @Tag("Parameterized")
    public void FarmerCreation2way(
            String country,
            @ConvertWith(NumberConverter.class) Integer age,
            String city,
            String ingredientsList,
            @ConvertWith(NumberConverter.class) Integer expected_outcome
    ) {
        int actual_outcome = 1;
        List<FoodEnum> inputIngredientsList;

        if (country.equals("None")) country = null;
        else if (country.equals("-")) country = "";

        if (city.equals("None")) city = null;
        else if (city.equals("-")) city = "";

        if (ingredientsList.equals("None")) inputIngredientsList = null;
        else inputIngredientsList = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);

        try {
            new VegetableFarmer("test", country, city, age,10, inputIngredientsList, null);
        } catch (IllegalArgumentException e) {
            actual_outcome = 0;
        }
        Assertions.assertEquals(expected_outcome, actual_outcome);
    }

    /**
     * This test assures the constructor of a Farmer class is correct, analyzing its parameters
     * The method for creating combinations is mixed strength method with city and country parameters prioritized.
     * @param country: the country location
     * @param age: age of this particular farmer
     * @param city: city he/she is located
     * @param ingredientsList: ingredients the farmer uses
     * @param expected_outcome: expected outcome of a test
     */
    @ParameterizedTest
    @CsvFileSource(resources = "/FarmerMixed.csv", delimiter = ',', numLinesToSkip = 1)
    @Tag("Parameterized")
    public void FarmerCreationMixed(
            String country,
            @ConvertWith(NumberConverter.class) Integer age,
            String city,
            String ingredientsList,
            @ConvertWith(NumberConverter.class) Integer expected_outcome
    ) {
        int actual_outcome = 1;
        List<FoodEnum> inputIngredientsList;

        if (country.equals("None")) country = null;
        else if (country.equals("-")) country = "";

        if (city.equals("None")) city = null;
        else if (city.equals("-")) city = "";

        if (ingredientsList.equals("None")) inputIngredientsList = null;
        else inputIngredientsList = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);

        try {
            new VegetableFarmer("test", country, city, age,10, inputIngredientsList, null);
        } catch (IllegalArgumentException e) {
            actual_outcome = 0;
        }
        Assertions.assertEquals(expected_outcome, actual_outcome);
    }

}
