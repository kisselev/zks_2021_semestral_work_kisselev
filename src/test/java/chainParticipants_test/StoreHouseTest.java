package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.Storehouse;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class StoreHouseTest {

    Storehouse storehouse;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);

    @Before
    public void prepare() {
        transactionInformer = new TransactionInformer();
        storehouse = new Storehouse("Store", 1000, transactionInformer);
    }

    /**
     * This test checks registering to all suitable channels for this type of ChainParticipant
     */
    @Test
    @Tag("Unittest")
    public void registerToTheChannelTest() {
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, storehouse);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        storehouse.registerToTheChannel();

        Assertions.assertTrue(VegetableChannel.getVegetableChannel().getCurrentParticipants().contains(storehouse));
        Assertions.assertTrue(MeatChannel.getMeatChannel().getCurrentParticipants().contains(storehouse));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().getCurrentParticipants().contains(storehouse));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().getCurrentParticipants().contains(storehouse));

    }

    /**
    * There are 6 input elements representing the cold fridge testing of the boundary values and
    * values with the smallest increment.
    * Specification:
    * -21: should fail because there is no fridge with the temperature below -20
    * -20, -19, -12, -12: should pass, because they lie within the range of temperatures of a cold fridge
    * -10: should fail the assertion, because the product will be placed into the medium fridge
    * */
    @ParameterizedTest
    @CsvSource({"-21, true", "-20, false", "-19, false", "-12, false", "-11, false", "-10, true"})
    @Tag("Parametrized")
    public void addFoodToColdFridgeTest(Integer storageTemperature, boolean shouldFail) {
        storehouse = new Storehouse("Store", 1000, transactionInformer);
        boolean result;
        try {
            FoodEntity entity = new FoodEntity(ICECREAM, "coldDessert", 1, storageTemperature,20);
            storehouse.addFood(entity);
            result = storehouse.getColdFridgeProducts().size() == 1;
        } catch (IllegalArgumentException e) {
            result = false;
        }
        Assertions.assertEquals(!shouldFail, result);
    }


    /**
     * There are 6 input elements representing the medium fridge testing of the boundary values and
     * values with the smallest increment.
     * Specification:
     * -11: should fail the assertion, because the product will be placed into the cold fridge
     * -10, -9, -2, -1: should pass, because they lie within the range of temperatures of a medium fridge
     * 0: should fail the assertion, because the product will be placed into the warm fridge
     * */
    @ParameterizedTest
    @CsvSource({"-11, true", "-10, false", "-9, false", "-2, false", "-1, false", "0, true"})
    @Tag("Parametrized")
    public void addFoodToMediumFridgeTest(Integer storageTemperature, boolean shouldFail) {
        storehouse = new Storehouse("Store", 1000, transactionInformer);
        boolean result;
        try {
            FoodEntity entity = new FoodEntity(BORSHCH, "readyMeal", 1, storageTemperature,200);
            storehouse.addFood(entity);
            result = storehouse.getMediumFridgeProducts().size() == 1;
        } catch (IllegalArgumentException e) {
            result = false;
        }
        Assertions.assertEquals(!shouldFail, result);
    }

    /**
     * There are 6 input elements representing the medium fridge testing of the boundary values and
     * values with the smallest increment.
     * Specification:
     * -1: should fail the assertion, because the product will be placed into the medium fridge
     * 0, 1, 9, 10: should pass, because they lie within the range of temperatures of a warm fridge
     * 11: should fail because there is no fridge with the temperature above 10 degrees
     * */
    @ParameterizedTest
    @CsvSource({"-1, true", "0, false", "1, false", "9, false", "10, false", "11, true"})
    @Tag("Parametrized")
    public void addFoodToWarmFridgeTest(Integer storageTemperature, boolean shouldFail) {
        storehouse = new Storehouse("Store", 1000, transactionInformer);
        boolean result;
        try {
            FoodEntity entity = new FoodEntity(SUGAR, "vegetable", 1, storageTemperature,360);
            storehouse.addFood(entity);
            result = storehouse.getWarmFridgeProducts().size() == 1;
        } catch (IllegalArgumentException e) {
            result = false;
        }
        Assertions.assertEquals(!shouldFail, result);
    }

    /**
    * This test is aimed to analyze the the behaviour of the system when processing the request for more elements
    * than are currently in the storage.
    * Specification: the request should return the available number of elements and ignore what's left
    * */
    @Test
    @Tag("Unittest")
    public void storageProcessMoreThanWeHaveTest() {
        FoodEntity entity = new FoodEntity(ICECREAM, "coldDessert", 1, -20,20);
        storehouse.addFood(entity);
        Request request = new Request(storehouse, ICECREAM, 2, storehouse.getOperationType());

        FoodEntity outputFoodEntity = storehouse.process(request);

        Assertions.assertEquals(1, outputFoodEntity.getQuantity());
    }

    /**
    * This test is aimed to test whether the products are correctly deleted from the fridge after processing the request
    * */
    @Test
    @Tag("Unittest")
    public void storageProcessEveryElementTest() {
        FoodEntity entity = new FoodEntity(BUTTER, "meat", 10, -11,400);
        storehouse.addFood(entity);
        Request request = new Request(storehouse, BUTTER, 10, storehouse.getOperationType());

        FoodEntity outputFoodEntity = storehouse.process(request);

        Assertions.assertEquals(10, outputFoodEntity.getQuantity());
        Assertions.assertEquals(0, storehouse.getProducts().size());
    }
}
