package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;


import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;


public class CustomerTest {
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    Manufacturer manufacturer;
    Customer customer1;

    @BeforeEach
    public void prepare() {
        manufacturer = new Manufacturer("Testing Retailer",1000, DRANIKI, transactionInformer);
        customer1 = new Customer("Test Customer1", 400, transactionInformer);

    }

    /**
     * This test checks registering to all suitable channels for this type of ChainParticipant
     */
    @Test
    @Tag("Unittest")
    public void registerToTheChannelTest() {
        customer1 = new Customer("Test Customer1", 400, transactionInformer);
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, customer1);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        customer1.registerToTheChannel();

        Assertions.assertTrue(VegetableChannel.getVegetableChannel().getCurrentParticipants().contains(customer1));
        Assertions.assertTrue(MeatChannel.getMeatChannel().getCurrentParticipants().contains(customer1));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().getCurrentParticipants().contains(customer1));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().getCurrentParticipants().contains(customer1));
    }

    /**
     * This test tests whether the food is added and the information on the transfer is updated.
     */
    @Test
    @Tag("Unittest")
    public void addFood() {
        customer1 = new Customer("Test Customer1", 400, transactionInformer);
        FoodEntity food = new FoodEntity(DRANIKI, "readyMeal", 8, 4,200);
        customer1.addFood(food);
        Assertions.assertTrue(customer1.getProducts().contains(food));
        Assertions.assertTrue(food.getDoneActions().contains(food + " with name " + food.getName() + " come to " + customer1));
    }

    /**
     * This test tests how this type of ChainParticipant processes the request
     */
    @Test
    @Tag("Unittest")
    public void processTest() {
        manufacturer = new Manufacturer("Testing Retailer",1000, DRANIKI, transactionInformer);
        customer1 = new Customer("Test Customer1", 400, transactionInformer);
        FoodEntity entity = new FoodEntity(ICECREAM, "coldDessert", 1, -20,20);
        customer1.addFood(entity);
        Request request = new Request(customer1, ICECREAM, 1, manufacturer.getOperationType());
        FoodEntity outputFoodEntity = customer1.process(request);
        Assertions.assertNull(outputFoodEntity);
    }


    /**
     * This test should not find any BORSHCH food entity and fail as the request only has ICECREAM
     */
    @Test
    @Tag("Unittest")
    public void processShouldFailTest() {
        customer1 = new Customer("Test Customer1", 400, transactionInformer);
        FoodEntity entity = new FoodEntity(ICECREAM, "coldDessert", 1, -20,20);
        customer1.addFood(entity);
        Request request = new Request(customer1, BORSHCH, 1, customer1.getOperationType());
        FoodEntity outputFoodEntity = customer1.process(request);
        Assertions.assertNull(outputFoodEntity);
    }
}
