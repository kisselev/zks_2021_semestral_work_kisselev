package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;


import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


public class RetailerTest {
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    Retailer retailer;

    @Before
    public void prepare() {
        retailer = new Retailer("Testing Retailer",1000, transactionInformer);
    }

    /**
     * This test checks registering to all suitable channels for this type of ChainParticipant
     */
    @Test
    @Tag("Unittest")
    public void registerToTheChannelTest() {
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, retailer);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        retailer.registerToTheChannel();

        Assertions.assertTrue(VegetableChannel.getVegetableChannel().getCurrentParticipants().contains(retailer));
        Assertions.assertTrue(MeatChannel.getMeatChannel().getCurrentParticipants().contains(retailer));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().getCurrentParticipants().contains(retailer));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().getCurrentParticipants().contains(retailer));
    }

    /**
     * This test tests whether the food is added and the information on the transfer is updated.
     */
    @Test
    @Tag("Unittest")
    public void addFood() {
        FoodEntity food = new FoodEntity(DRANIKI, "readyMeal", 8, 4,200);
        retailer.addFood(food);
        Assertions.assertTrue(retailer.getProducts().contains(food));
        Assertions.assertTrue(food.getDoneActions().contains(food + " with name " + food.getName() + " come to " + retailer));
    }

    /**
     * This test assures the requests are processed right by the retailer.
     */
    @Test
    @Tag("Unittest")
    public void processTest() {
        FoodEntity entity = new FoodEntity(ICECREAM, "coldDessert", 1, -20,20);
        retailer.addFood(entity);
        Request request = new Request(retailer, ICECREAM, 1, retailer.getOperationType());
        FoodEntity outputFoodEntity = retailer.process(request);
        Assertions.assertEquals(1, outputFoodEntity.getQuantity());
    }


    /**
     * This test should not find any BORSHCH food entity and fail
     * */
    @Test
    @Tag("Unittest")
    public void processShouldFailTest() {
        FoodEntity entity = new FoodEntity(ICECREAM, "coldDessert", 1, -20,20);
        retailer.addFood(entity);
        Request request = new Request(retailer, BORSHCH, 1, retailer.getOperationType());
        FoodEntity outputFoodEntity = retailer.process(request);
        Assertions.assertNull(outputFoodEntity);
    }

}
