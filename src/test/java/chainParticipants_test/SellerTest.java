package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;


import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;



public class SellerTest {

    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    Seller seller;

    @Before
    public void prepare() {
        seller = new Seller("Testing Seller",1000, transactionInformer);
    }

    /**
     * This test checks registering to all suitable channels for this type of ChainParticipant
     */
    @Test
    public void registerToTheChannelTest() {
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, seller);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        seller.registerToTheChannel();

        Assertions.assertTrue(VegetableChannel.getVegetableChannel().getCurrentParticipants().contains(seller));
        Assertions.assertTrue(MeatChannel.getMeatChannel().getCurrentParticipants().contains(seller));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().getCurrentParticipants().contains(seller));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().getCurrentParticipants().contains(seller));
    }

    /**
     * This test tests whether the food is added and the information on the transfer is updated.
     */
    @Test
    public void addFood() {
        FoodEntity food = new FoodEntity(DRANIKI, "readyMeal", 8, 4,200);
        seller.addFood(food);
        Assertions.assertTrue(seller.getProducts().contains(food));
        Assertions.assertTrue(food.getDoneActions().contains(food + " with name " + food.getName() + " come to " + seller));
    }

    /**
     * This test assures the requests are processed right by the retailer.
     */
    @Test
    @Tag("Unittest")
    public void processTest() {
        FoodEntity entity = new FoodEntity(ICECREAM, "coldDessert", 1, -20,20);
        seller.addFood(entity);
        Request request = new Request(seller, ICECREAM, 1, seller.getOperationType());
        FoodEntity outputFoodEntity = seller.process(request);
        Assertions.assertEquals(1, outputFoodEntity.getQuantity());
    }


    /**
    * This test should not find any BORSHCH food entity and fail
    */
    @Test
    @Tag("Unittest")
    public void processShouldFailTest() {
        FoodEntity entity = new FoodEntity(ICECREAM, "coldDessert", 1, -20,20);
        seller.addFood(entity);
        Request request = new Request(seller, BORSHCH, 1, seller.getOperationType());
        FoodEntity outputFoodEntity = seller.process(request);
        Assertions.assertNull(outputFoodEntity);
    }
}
