package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.Farmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Seller;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import input_converters.NumberConverter;
import input_converters.StringConverter;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.ICECREAM;

public class ChainParticipantParametrizedTest {
    List<FoodEnum> vegetableIngredients;

    /**
     * This test assures the constructor of a ChainParticipant class is correct, analyzing its parameters
     * @param name: the name of a ChainParticipant
     * @param cashAccount: the amount of cash he/her possesses
     * @param phoneNumber: her/his phone number
     * @param expected_outcome: the expected outcome of a test
     */
    @ParameterizedTest
    @CsvFileSource(resources = "/ChainParticipant2way.csv", delimiter = ',', numLinesToSkip = 1)
    @Tag("Parameterized")
    public void ChainParticipantCreation2way(
            String name,
            @ConvertWith(NumberConverter.class) Integer cashAccount,
            String phoneNumber,
            @ConvertWith(NumberConverter.class) Integer expected_outcome
    ) {
        vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
        int actual_outcome = 1;
        List<FoodEnum> inputIngredientsList;

        if (name.equals("None")) name = null;
        else if (name.equals("-")) name = "";

        if (phoneNumber.equals("None")) phoneNumber = null;

        try {
            new Seller(name, cashAccount, phoneNumber, null);
        } catch (IllegalArgumentException e) {
            actual_outcome = 0;
        }
        Assertions.assertEquals(expected_outcome, actual_outcome);
    }
}
