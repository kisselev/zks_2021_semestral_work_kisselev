package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;


import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import static org.junit.Assert.*;

public class VegetableFarmerTest {
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    VegetableFarmer vegetableFarmer;
    private PriceWriter visitor;
    private TransactionInformer transactionInformer;

    private int getQuantity(List<FoodEntity> entities) {
        int sum = 0;
        for (FoodEntity entity: entities){
            sum += entity.getQuantity();
        }
        return sum;
    }

    @Before
    public void prepare() {
        visitor = new PriceWriter();
        transactionInformer = new TransactionInformer();
        vegetableFarmer = new VegetableFarmer("Test Farmer", "Test Country", 2000, vegetableIngredients, transactionInformer);
    }

    /**
     * This test assures the requests are processed right by the vegetable farmer.
     */
    @Test
    @Tag("Unittest")
    public void processTest() {
        Request request = new Request(vegetableFarmer, POTATO, 1, vegetableFarmer.getOperationType());
        FoodEntity request_result = vegetableFarmer.process(request);
        assertEquals(POTATO, request_result.getName());
        assertEquals(1, request_result.getQuantity());
    }

    /**
     * This test checks registering to all suitable channels for this type of ChainParticipant
     */
    @Test
    @Tag("Unittest")
    public void registerToChannelTest() {
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, vegetableFarmer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        vegetableFarmer.registerToTheChannel();

        assertEquals(VegetableChannel.getVegetableChannel().getCurrentParticipants(), participants);
        assertEquals(PaymentChannel.getPaymentChannel().getCurrentParticipants(), participants);
    }

    /**
     * This test accepts a visitor and checks whether it is working by assuring the prices are set correctly.
     */
    @Test
    @Tag("Unittest")
    public void acceptTest() {
        vegetableFarmer.accept(visitor);

        ArrayList<Integer> setPrices = new ArrayList<>();
        for (FoodEnum food_element: vegetableIngredients) {
            setPrices.add(vegetableFarmer.askPrice(food_element, 1));
        }
        List<Integer> list = Arrays.asList(18, 10, 20, 25, 15, 12);
        assertEquals(list, setPrices);
    }

    /**
     * This test processes several request at once, checking the application for correctness dealing with multiple data
     * inputs.
     */
    @Test
    @Tag("Unittest")
    public void processSeveralRequestsTest() {
        Request request1 = new Request(vegetableFarmer, POTATO, 1, vegetableFarmer.getOperationType());
        Request request2 = new Request(vegetableFarmer, BEET, 3, vegetableFarmer.getOperationType());
        Request request3 = new Request(vegetableFarmer, ONION, 4, vegetableFarmer.getOperationType());

        FoodEntity request_result1 = vegetableFarmer.process(request1);
        FoodEntity request_result2 = vegetableFarmer.process(request2);
        FoodEntity request_result3 = vegetableFarmer.process(request3);

        List<FoodEntity> entities = Arrays.asList(request_result1, request_result2, request_result3);
        assertEquals(POTATO, request_result1.getName());
        assertEquals(8, getQuantity(entities));
    }
}
