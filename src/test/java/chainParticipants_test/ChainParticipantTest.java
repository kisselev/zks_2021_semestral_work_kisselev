package chainParticipants_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.Transaction;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class ChainParticipantTest {

    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    Manufacturer manufacturer;
    VegetableFarmer vegetableFarmer;
    Customer customer1;


    @BeforeEach
    public void prepare() {
        transactionInformer = new TransactionInformer();
        manufacturer = new Manufacturer("Testing Retailer",1000, BORSHCH, transactionInformer);
        vegetableFarmer = new VegetableFarmer("Test Farmer", "Test Country", 2000, vegetableIngredients, transactionInformer);
        customer1 = new Customer("Test Customer1", 400, transactionInformer);

        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, vegetableFarmer, manufacturer, customer1);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        manufacturer.registerToTheChannel();
        customer1.registerToTheChannel();
        vegetableFarmer.registerToTheChannel();
    }

    /**
     * This test checks the correctness of sending a request.
     */
    @Test
    @Tag("Unittest")
    public void sendRequestTest() {
        // Arrange
        FoodEnum testEnum = ONION;

        // Act
        customer1.sendRequest(testEnum, 1);

        // Assert
        Assertions.assertTrue(customer1.getProducts().stream().anyMatch(i -> i.getName().equals(testEnum)));
        Assertions.assertEquals(1, customer1.getProducts().size());
    }

    /**
     * This test checks the correctness of sending a request in case there is no available executor. The food entity
     * shouldn't be added.
     */
    @Test
    @Tag("Unittest")
    public void sendRequestNoExecutorTest() {
        // Arrange
        FoodEnum testEnum = MEAT;

        // Act
        customer1.sendRequest(testEnum, 1);

        // Assert
        Assertions.assertFalse(customer1.getProducts().stream().anyMatch(i -> i.getName().equals(testEnum)));
        Assertions.assertEquals(0, customer1.getProducts().size());
    }


    /**
     * This test is aimed to check the process of adding a new transaction.
     */
    @Test
    @Tag("Unittest")
    public void updateTransactionsTest() {
        // Arrange
        Request request = new Request(customer1, BEET, 1, vegetableFarmer.getOperationType());
        Transaction new_transaction1 = new Transaction(request, vegetableFarmer, customer1.getLastHash());

        // Act
        boolean customer_update = customer1.updateTransactions(new_transaction1);
        boolean farmer_update = vegetableFarmer.updateTransactions(new_transaction1);

        Assertions.assertTrue(customer_update);
        Assertions.assertTrue(farmer_update);
        Assertions.assertEquals(new_transaction1.getHash(), customer1.getLastHash());
        Assertions.assertEquals(new_transaction1.getHash(), vegetableFarmer.getLastHash());
    }

    /**
     * This test is aimed to check the process of adding a new transaction but with a changed hash.
     */
    @Test
    @Tag("Unittest")
    public void updateTransactionsHashChangedTest() {
        // Arrange
        Request request1 = new Request(customer1, BEET, 1, vegetableFarmer.getOperationType());
        Transaction new_transaction1 = new Transaction(request1, vegetableFarmer, customer1.getLastHash());

        // Act
        customer1.updateTransactions(new_transaction1);
        vegetableFarmer.updateTransactions(new_transaction1);

        Request request2 = new Request(customer1, BEET, 1, vegetableFarmer.getOperationType());
        Transaction new_transaction2 = new Transaction(request2, vegetableFarmer, customer1.getLastHash() + 10);

        boolean customer_update = customer1.updateTransactions(new_transaction2);
        boolean farmer_update = vegetableFarmer.updateTransactions(new_transaction2);

        Assertions.assertFalse(customer_update);
        Assertions.assertFalse(farmer_update);
    }

    /**
     * This test simulates a double spending problem when the hash is the same for two transactions.
     */
    @Test
    @Tag("Unittest")
    public void doubleSpendingTest() {
        // Arrange
        Request request = new Request(customer1, BEET, 1, vegetableFarmer.getOperationType());
        Transaction new_transaction = new Transaction(request, vegetableFarmer, customer1.getLastHash());
        customer1.updateTransactions(new_transaction);

        // Act
        boolean double_spending_check = customer1.doubleSpending(new_transaction);

        // Assert
        Assertions.assertTrue(double_spending_check);
    }
}