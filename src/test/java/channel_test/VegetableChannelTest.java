package channel_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;

public class VegetableChannelTest {
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    TransactionInformer transactionInformer;
    VegetableFarmer vegetableFarmer;
    Customer customer1;
    Customer customer2;
    List<Party> participants;

    @Before
    public void prepare() {
        transactionInformer = new TransactionInformer();
        vegetableFarmer = new VegetableFarmer("Test Vegetable Farmer",
                "Test Vegetable Country",
                2000,
                vegetableIngredients,
                transactionInformer);
        customer1 = new Customer("Test Customer1", 400, transactionInformer);
        customer2 = new Customer("Test Customer2", 0, transactionInformer);
        participants = new ArrayList<>();
        Collections.addAll(participants, vegetableFarmer, customer1, customer2);
    }

    /**
     * Test that checks the registering process of a farmer being assigned to a channel
     */
    @Test
    @Tag("Unittest")
    public void registerTest() {
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, vegetableFarmer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer).register(vegetableFarmer);

        Assertions.assertTrue(VegetableChannel.getVegetableChannel().getCurrentParticipants().contains(vegetableFarmer));
    }

    /**
    * Make and execute a request for a vegetable product. Check, whether the request is successfully processed
    */
    @Test
    @Tag("Unittest")
    public void makeBasicRequestTest() {
        Request request = new Request(customer1, BEET, 10, vegetableFarmer.getOperationType());
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer).register(vegetableFarmer);
        VegetableChannel.getVegetableChannel().register(customer1);

        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        PaymentChannel.getPaymentChannel().register(customer1);
        PaymentChannel.getPaymentChannel().register(vegetableFarmer);

        VegetableChannel.getVegetableChannel().makeRequest(request);

        Assertions.assertTrue(customer1.getProducts().stream().anyMatch(i -> i.getName().equals(BEET)));
    }

    /**
     * Make and execute a request for a meat product for a vegetable farmer, to check the handling of error requests.
     * Checks, whether the request is unsuccessful.
     */
    @Test
    @Tag("Unittest")
    public void makeWrongRequestTest(){
        Request request = new Request(customer1, MEAT, 10, vegetableFarmer.getOperationType());
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer).register(vegetableFarmer);
        VegetableChannel.getVegetableChannel().register(customer1);

        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        PaymentChannel.getPaymentChannel().register(customer1);
        PaymentChannel.getPaymentChannel().register(vegetableFarmer);

        VegetableChannel.getVegetableChannel().makeRequest(request);

        Assertions.assertFalse(customer1.getProducts().stream().anyMatch(i -> i.getName().equals(MEAT)));
        Assertions.assertEquals(0, customer1.getProducts().size());
    }

    /**
     * Make and execute a request for a product without enough money on the account, to check the handling of error requests.
     * Checks, whether the request is unsuccessful.
     */
    @Test
    @Tag("Unittest")
    public void makeRequestNotEnoughMoneyTest() {
        Request request = new Request(customer2, ONION, 10, vegetableFarmer.getOperationType());
        vegetableFarmer.put(ONION, 10);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer).register(vegetableFarmer);
        VegetableChannel.getVegetableChannel().register(customer2);

        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        PaymentChannel.getPaymentChannel().register(customer2);
        PaymentChannel.getPaymentChannel().register(vegetableFarmer);

        VegetableChannel.getVegetableChannel().makeRequest(request);

        Assertions.assertFalse(customer2.getProducts().stream().anyMatch(i -> i.getName().equals(MEAT)));
        Assertions.assertEquals(0, customer2.getProducts().size());
    }
}
