package channel_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;


public class PaymentChannelTest {

    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<Party> participants;
    TransactionInformer transactionInformer;
    MeatFarmer meatFarmer;
    VegetableFarmer vegetableFarmer;
    Manufacturer manufacturerBorshch;
    Manufacturer manufacturerDraniki;
    Manufacturer manufacturerIceCream;
    Manufacturer manufacturerKyivCutlet;
    Manufacturer manufacturerPancakes;
    Storehouse storehouse;
    Seller seller;
    Retailer retailer;
    Customer customer1;
    Customer customer2;

    @Before
    public void prepare() {
        transactionInformer = new TransactionInformer();
        meatFarmer = new MeatFarmer("Test Meat Farmer", "Test Country", 1000, meatIngredients, transactionInformer);
        vegetableFarmer = new VegetableFarmer("Test Vege Farmer", "Test Country", 2000, vegetableIngredients, transactionInformer);
        manufacturerBorshch = new Manufacturer("Test Manufacturer1", 1000, BORSHCH, transactionInformer);
        manufacturerDraniki = new Manufacturer("Test Manufacturer2", 1000, DRANIKI, transactionInformer);
        manufacturerIceCream = new Manufacturer("Test Manufacturer3", 1000, ICECREAM, transactionInformer);
        manufacturerKyivCutlet = new Manufacturer("Test Manufacturer4", 1000, KYIVCUTLET, transactionInformer);
        manufacturerPancakes = new Manufacturer("Test Manufacturer5", 1000, PANCAKES, transactionInformer);
        storehouse = new Storehouse("Test Store", 1000, transactionInformer);
        seller = new Seller("Test Seller",1000, transactionInformer);
        retailer = new Retailer("Test Retailer",1000, transactionInformer);
        customer1 = new Customer("Test Customer1", 400, transactionInformer);
        customer2 = new Customer("Test Customer2", 1000, transactionInformer);
        participants = new ArrayList<>();
        Collections.addAll(participants,
                meatFarmer,
                vegetableFarmer,
                manufacturerBorshch,
                manufacturerDraniki,
                manufacturerIceCream,
                manufacturerKyivCutlet,
                manufacturerPancakes,
                storehouse,
                seller,
                retailer,
                customer1,
                customer2);
    }

    /**
    * Payment channel has a wider scope of the allowed participants, test if it is able to register them all
    * */
    @Test
    @Tag("Unittest")
    public void registerTest() {
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        for (Party participant: participants) {
            PaymentChannel.getPaymentChannel().register(participant);
        }
        Assertions.assertEquals(12, PaymentChannel.getPaymentChannel().getCurrentParticipants().size());
        Assertions.assertEquals(participants, PaymentChannel.getPaymentChannel().getCurrentParticipants());
    }

    /**
     * The test assures the process of unregistering the participants is working correctly
     * */
    @Test
    @Tag("Unittest")
    public void unregisterTest() {
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        for (Party participant: participants) {
            PaymentChannel.getPaymentChannel().register(participant);
        }
        PaymentChannel.getPaymentChannel().unregister(storehouse);
        PaymentChannel.getPaymentChannel().unregister(meatFarmer);
        PaymentChannel.getPaymentChannel().unregister(vegetableFarmer);
        Assertions.assertEquals(9, PaymentChannel.getPaymentChannel().getCurrentParticipants().size());
    }

    /**
     * The test assures the process of making payment between the participants is working correctly, the money are
     * transferred right.
     * */
    @Test
    @Tag("Unittest")
    public void makePaymentTest() {
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        PaymentChannel.getPaymentChannel().register(customer1);
        PaymentChannel.getPaymentChannel().register(meatFarmer);
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(customer1));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(meatFarmer));


        PaymentChannel.getPaymentChannel().makePayment(customer1, meatFarmer, 100);
        Assertions.assertEquals(300, customer1.getCashAccount());
        Assertions.assertEquals(1100, meatFarmer.getCashAccount());
    }

    /**
    * This test analyses the situation when the cashAccount does not have sufficient amount of money, the transaction
    * is denied, the cashAccounts of both participants should stay the same
    */
    @Test
    @Tag("Unittest")
    public void shouldNotMakePaymentTest() {
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        PaymentChannel.getPaymentChannel().register(customer2);
        PaymentChannel.getPaymentChannel().register(manufacturerBorshch);
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(customer2));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(manufacturerBorshch));


        PaymentChannel.getPaymentChannel().makePayment(customer2, manufacturerBorshch, 2000);
        Assertions.assertEquals(1000, customer2.getCashAccount());
        Assertions.assertEquals(1000, manufacturerBorshch.getCashAccount());
    }

    /**
     * The test simulates putting money to the channel and assures they are there
     */
    @Test
    @Tag("Unittest")
    public void putMoneyTest() {
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        PaymentChannel.getPaymentChannel().register(customer1);
        PaymentChannel.getPaymentChannel().register(vegetableFarmer);
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(customer1));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(vegetableFarmer));

        PaymentChannel.getPaymentChannel().setApplicant(customer1);

        PaymentChannel.getPaymentChannel().putMoney(customer1, 400);
        Assertions.assertEquals(400, PaymentChannel.getPaymentChannel().getAmount());
    }
}
