package channel_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.operations.Request;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class ReadyMealChannelTest {

    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);

    TransactionInformer transactionInformer;
    Storehouse storehouse;
    Customer customer1;
    Customer customer2;
    List<Party> participants;


    @Before
    public void prepare() {
        transactionInformer = new TransactionInformer();
        storehouse = new Storehouse("Test Store", 1000, transactionInformer);
        storehouse.put(BORSHCH, 200);
        customer1 = new Customer("Test Customer1", 400, transactionInformer);
        customer2 = new Customer("Test Customer2", 0, transactionInformer);
        participants = new ArrayList<>();
        Collections.addAll(participants,
                storehouse,
                customer1,
                customer2);
    }

    /**
    * Arrange both channels, food and payment, prepare request, food entity and try to execute request
    * */
    @Test
    @Tag("Unittest")
    public void executeRequestTest() {
        // Arrange the ReadyMeal channel, set applicant and executor
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        ReadyMealChannel.getReadyMealChannel().setApplicant(customer1);
        ReadyMealChannel.getReadyMealChannel().setExecutor(storehouse);

        // Arrange the Payment channel, register the applicant and executor
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        PaymentChannel.getPaymentChannel().register(storehouse);
        PaymentChannel.getPaymentChannel().register(customer1);

        // Create a request and set price
        Request request = new Request(storehouse, BORSHCH, 2, storehouse.getOperationType());
        request.setPrice(storehouse.askPrice(request.getFoodEnum(), request.getQuantity()));

        // Create a food entity to sell
        FoodEntity foodEntity = new FoodEntity(BORSHCH, "readyMeal", 1, 8,20);

        // Set the entity in the ReadyMealChannel
        ReadyMealChannel.getReadyMealChannel().setFoodEntity(foodEntity);

        // Act: execute the function that is being tested
        ReadyMealChannel.getReadyMealChannel().executeRequest(request);

        // Assertions
        Assertions.assertEquals(1, customer1.getProducts().size());
        Assertions.assertTrue(customer1.getProducts().contains(foodEntity));

    }

    /**
     * Arrange both channels, food and payment, prepare request, food entity and try to execute request without enough
     * money.
     * */
    @Test
    @Tag("Unittest")
    public void executeRequestNoMoneyTest() {
        // Arrange the ReadyMeal channel, set applicant and executor
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        ReadyMealChannel.getReadyMealChannel().setApplicant(customer2);
        ReadyMealChannel.getReadyMealChannel().setExecutor(storehouse);

        // Arrange the Payment channel, register the applicant and executor
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        PaymentChannel.getPaymentChannel().register(storehouse);
        PaymentChannel.getPaymentChannel().register(customer2);

        // Create a request and set price
        Request request = new Request(storehouse, BORSHCH, 2, storehouse.getOperationType());
        request.setPrice(storehouse.askPrice(request.getFoodEnum(), request.getQuantity()));

        // Create a food entity to sell
        FoodEntity foodEntity = new FoodEntity(BORSHCH, "readyMeal", 1, 8,20);

        // Set the entity in the ReadyMealChannel
        ReadyMealChannel.getReadyMealChannel().setFoodEntity(foodEntity);

        // Act: execute the function that is being tested
        ReadyMealChannel.getReadyMealChannel().executeRequest(request);

        // Assertions
        Assertions.assertEquals(0, customer2.getProducts().size());
        Assertions.assertFalse(customer2.getProducts().contains(foodEntity));
    }

    /**
     * The test tries to unregister a participant currently involved in a transaction.
     */
    @Test
    @Tag("Unittest")
    public void unregisterWhileInvolvedInTransactionTest(){
        // Arrange: register applicant and executor as participants
        boolean unregister_result;
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        ReadyMealChannel.getReadyMealChannel().register(customer1);
        ReadyMealChannel.getReadyMealChannel().register(storehouse);
        ReadyMealChannel.getReadyMealChannel().setApplicant(customer1);
        ReadyMealChannel.getReadyMealChannel().setExecutor(storehouse);

        // Act: try to unregister
        unregister_result = ReadyMealChannel.getReadyMealChannel().unregister(customer1);
        Assertions.assertFalse(unregister_result);

        unregister_result = ReadyMealChannel.getReadyMealChannel().unregister(storehouse);
        Assertions.assertFalse(unregister_result);
    }

    /**
     * The test assures the process of unregistering the participants is working correctly
     * */
    @Test
    @Tag("Unittest")
    public void unregisterTest() {
        boolean unregister_result;
        PaymentChannel.getPaymentChannel(participants, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        ReadyMealChannel.getReadyMealChannel().register(customer1);
        ReadyMealChannel.getReadyMealChannel().register(storehouse);

        // Act: try to unregister
        unregister_result = ReadyMealChannel.getReadyMealChannel().unregister(customer1);
        Assertions.assertTrue(unregister_result);

        unregister_result = ReadyMealChannel.getReadyMealChannel().unregister(storehouse);
        Assertions.assertTrue(unregister_result);
    }
}
