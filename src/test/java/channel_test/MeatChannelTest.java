package channel_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.operations.Request;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class MeatChannelTest {

    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    TransactionInformer transactionInformer;

    @Before
    public void prepare() {
        transactionInformer = new TransactionInformer();
    }

    /**
     * Test that checks the registering process of a wrong farmer being assigned to a channel that does not accept this
     * type of farmer
     * */
    @Test
    @Tag("Unittest")
    public void shouldNotRegisterTest() {
        VegetableFarmer vegetableFarmer = new VegetableFarmer("Test Vegetables Farmer",
                "Test Vegetable Country",
                2000, vegetableIngredients, transactionInformer);

        MeatFarmer meatFarmer = new MeatFarmer("Test Meat Farmer",
                "Test Meat Country",
                1000,
                meatIngredients,
                transactionInformer);

        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, meatFarmer);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer).register(vegetableFarmer);

        Assertions.assertFalse(MeatChannel.getMeatChannel().getCurrentParticipants().contains(vegetableFarmer));
    }

    /**
     * Test that checks the registering process of a farmer being assigned to a channel
     * */
    @Test
    @Tag("Unittest")
    public void shouldRegisterTest() {
        MeatFarmer meatFarmer = new MeatFarmer("Test Meat Farmer", "Test Meat Country", 1000, meatIngredients, transactionInformer);

        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, meatFarmer);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer).register(meatFarmer);

        Assertions.assertTrue(MeatChannel.getMeatChannel().getCurrentParticipants().contains(meatFarmer));
    }

    /**
     * Test that checks the process of finding the executor for a meat request works correctly
     * */
    @Test
    @Tag("Unittest")
    public void shouldFindExecutor() {
        // Arrange
        MeatFarmer meatFarmer = new MeatFarmer("Test Meat Farmer",
                "Test Meat Country",
                1000,
                meatIngredients,
                transactionInformer);
        VegetableFarmer vegetableFarmer = new VegetableFarmer("Test Vegetables Farmer",
                "Test Vegetable Country",
                2000, vegetableIngredients, transactionInformer);

        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, meatFarmer, vegetableFarmer);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer).register(meatFarmer);
        Request request = new Request(meatFarmer, BUTTER, 2, meatFarmer.getOperationType());

        // Act
        Party executor = MeatChannel.getMeatChannel().findExecutor(request);

        // Assert
        Assertions.assertNotNull(executor);
    }


    /**
    * Test that checks whether the rights for participating in the meat delivery are assigned
    * */
    @Test
    @Tag("Unittest")
    public void shouldNotFindExecutor() {
        // Arrange
        MeatFarmer meatFarmer = new MeatFarmer("Test Meat Farmer",
                "Test Meat Country",
                1000,
                meatIngredients,
                transactionInformer);
        VegetableFarmer vegetableFarmer = new VegetableFarmer("Test Vegetables Farmer",
                "Test Vegetable Country",
                2000, vegetableIngredients, transactionInformer);

        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, vegetableFarmer);
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer).register(meatFarmer);
        Request request = new Request(meatFarmer, BUTTER, 2, meatFarmer.getOperationType());

        // Act
        Party executor = MeatChannel.getMeatChannel().findExecutor(request);

        // Assert
        Assertions.assertNull(executor);
    }

}