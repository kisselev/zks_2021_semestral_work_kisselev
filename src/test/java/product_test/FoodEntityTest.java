package product_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.states.FinalPackedState;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.Assert.fail;


public class FoodEntityTest {
    List<FoodEnum> meatIngredients;
    List<FoodEnum> vegetableIngredients;
    List<FoodEnum> readyMeals;
    List<FoodEnum> coldDessert;
    TransactionInformer transactionInformer;
    VegetableFarmer vegetableFarmer;
    Customer customer1;

    private FoodEnum getRandomFood(List<FoodEnum> foodCategoryMembers) {
        Random rand = new Random();
        return foodCategoryMembers.get(rand.nextInt(foodCategoryMembers.size()));
    }

    @Before
    public void prepare() {
        transactionInformer = new TransactionInformer();
        meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
        vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
        readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, BORSHCH);
        coldDessert = Arrays.asList(TIRAMISU, ICECREAM);
        vegetableFarmer = new VegetableFarmer("Test Vegetable Farmer",
                "Test Vegetable Country",
                2000,
                vegetableIngredients,
                transactionInformer);
        customer1 = new Customer("Test Customer1", 400, transactionInformer);
    }

    /**
     * Tests that packs a food entity for transportation it to a customer: therefore the packaging is final
     */
    @Test
    @Tag("Unittest")
    public void shouldPackForCustomerTest() {
        // Arrange
        FoodEntity testEntity = new FoodEntity(BEET, 1, 4, 50);

        // Act
        testEntity.transport(customer1);

        // Assert
        Assertions.assertEquals("final", testEntity.getStringState());
    }

    /**
     * Tests that packs a food entity for transportation it to a farmer: therefore the packaging is final
     */
    @Test
    @Tag("Unittest")
    public void shouldPackForTemporaryTransportingTest() {
        // Arrange
        FoodEntity testEntity = new FoodEntity(BEET, 1, 4, 50);

        // Act
        testEntity.transport(vegetableFarmer);

        // Assert
        Assertions.assertEquals("temporary", testEntity.getStringState());
    }

    /**
     * Assures that useForCooking method unpacks the package of a food entity
     */
    @Test
    @Tag("Unittest")
    public void useForCookingBeforeTransportTest() {
        // Arrange
        FoodEntity testEntity = new FoodEntity(BEET, 1, 4, 50);
        testEntity.transport(customer1);
        Assertions.assertEquals("final", testEntity.getStringState());

        // Act
        testEntity.useForCooking();

        // Assert
        Assertions.assertEquals("unpacked", testEntity.getStringState());
    }

    /**
     * Writes all performed actions to a text file, reopens it and checks whether it contains everything
     */
    @Test
    @Tag("Unittest")
    public void writeAllActionsTextTest() {
        // Arrange
        FoodEntity testEntity = new FoodEntity(BEET, 1, 4, 50);
        String testAction = "BEET entity has been created and packed";
        testEntity.addAction(testAction);
        String fileName = "TestTransactionReport.txt";

        // Act
        testEntity.writeAllActionsText(fileName);


        File testFile = new File(fileName);
        boolean exists = testFile.exists();

        Assertions.assertTrue(exists);

        try {
            Scanner scanner = new Scanner(testFile);
            scanner.nextLine();
            Assertions.assertEquals(scanner.nextLine(), testAction);
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}


