package product_test;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import input_converters.StringConverter;
import input_converters.NumberConverter;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.Assert.fail;


public class FoodEntityParametrizedTest {
    List<FoodEnum> meatIngredients;
    List<FoodEnum> vegetableIngredients;
    List<FoodEnum> readyMeals;
    List<FoodEnum> coldDessert;

    private FoodEnum getRandomFood(List<FoodEnum> foodCategoryMembers) {
        Random rand = new Random();
        return foodCategoryMembers.get(rand.nextInt(foodCategoryMembers.size()));
    }

    @Before
    public void prepare() {
        meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
        vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
        readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, BORSHCH);
        coldDessert = Arrays.asList(TIRAMISU, ICECREAM);
    }

    /**
     * This test assures the constructor of a FoodEntity class is correct, analyzing its parameters
     * The method for creating combinations is 3 way method.
     * @param foodName: a name of a dish/ingredient
     * @param storageTemperature: storage temperature of a dish/ingredient
     * @param quantity: the number of elements to create
     * @param calories: calories information on this entity
     * @param expected_outcome: expected outcome of a test
     */
    @ParameterizedTest
    @CsvFileSource(resources = "/FoodEntity3way.csv", delimiter = ',', numLinesToSkip = 1)
    @Tag("Parameterized")
    public void FoodEntityCreation3way(
            String foodName,
            @ConvertWith(NumberConverter.class) Integer storageTemperature,
            @ConvertWith(NumberConverter.class) Integer quantity,
            @ConvertWith(NumberConverter.class) Integer calories,
            @ConvertWith(NumberConverter.class) Integer expected_outcome
    ) {
        int actual_outcome = 1;
        FoodEnum testEnum;
        if (foodName.equals("None")) testEnum = null;
        else testEnum = POTATO;

        try {
            new FoodEntity(testEnum, quantity, storageTemperature, calories);
        } catch (IllegalArgumentException e) {
            actual_outcome = 0;
        }
        Assertions.assertEquals(expected_outcome, actual_outcome);
    }

    /**
     * This test assures the constructor of a FoodEntity class is correct, analyzing its parameters
     * The method for creating combinations is 2 way (pairwise) method.
     * @param foodName: a name of a dish/ingredient
     * @param storageTemperature: storage temperature of a dish/ingredient
     * @param quantity: the number of elements to create
     * @param calories: calories information on this entity
     * @param expected_outcome: expected outcome of a test
     */
    @ParameterizedTest
    @CsvFileSource(resources = "/FoodEntity2way.csv", delimiter = ',', numLinesToSkip = 1)
    @Tag("Parameterized")
    public void FoodEntityCreationPairwise(
            String foodName,
            @ConvertWith(NumberConverter.class) Integer storageTemperature,
            @ConvertWith(NumberConverter.class) Integer quantity,
            @ConvertWith(NumberConverter.class) Integer calories,
            @ConvertWith(NumberConverter.class) Integer expected_outcome
    ) {
        int actual_outcome = 1;
        FoodEnum testEnum;
        if (foodName.equals("None")) testEnum = null;
        else testEnum = POTATO;

        try {
            new FoodEntity(testEnum, quantity, storageTemperature, calories);
        } catch (IllegalArgumentException e) {
            actual_outcome = 0;
        }
        Assertions.assertEquals(expected_outcome, actual_outcome);
    }
}
