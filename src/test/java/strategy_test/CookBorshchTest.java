package strategy_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.strategy.CookBorshch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.*;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.MEAT;
import static org.junit.jupiter.api.Assertions.*;

class CookBorshchTest {

    TransactionInformer transactionInformer;
    MeatFarmer meatFarmer;
    VegetableFarmer vegetableFarmer;
    Manufacturer manufacturer;
    CookBorshch cookBorshch;
    Set<FoodEnum> receipt;

    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);


    @BeforeEach
    public void prepare(){
        transactionInformer = new TransactionInformer();
        // Create participants
        List<Party> participants = new ArrayList<>();
        manufacturer = new Manufacturer("Test Borshch Manufacturer", 1000, BORSHCH, transactionInformer);

        meatFarmer = new MeatFarmer("Test Meat Farmer", "Test Country", 1000, meatIngredients, transactionInformer);
        vegetableFarmer = new VegetableFarmer("Test Vege Farmer", "Test Country", 2000, vegetableIngredients, transactionInformer);
        Collections.addAll(participants, meatFarmer, vegetableFarmer, manufacturer);

        // Arrange the channels
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        // Register
        manufacturer.registerToTheChannel();
        vegetableFarmer.registerToTheChannel();


        // Cook Borshch receipt
        cookBorshch = new CookBorshch(manufacturer);
        receipt = new HashSet<>(Arrays.asList(BEET, MEAT, POTATO, WATER));
    }

    /**
     * The test fills the prescribed recipe and cooks a dish from the ingredients
     */
    @Test
    public void fillReceiptTest(){
        // Arrange
        manufacturer.addFood(new FoodEntity(BEET,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(MEAT,"meat", 1, -15, 200));
        manufacturer.addFood(new FoodEntity(POTATO,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(WATER, "water", 1, 20, 0));

        // Act
        cookBorshch.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookBorshch.getReceipt();

        // Assert
        for(FoodEnum ingredient : receipt){
            assertNotNull(filledReceipt.get(ingredient));
        }
    }

    /**
     * The test fills the prescribed recipe leaving some of the ingredients missing and tries to cook a dish
     */
    @Test
    @Tag("Unittest")
    public void fillReceiptNotEnoughItemsTest(){
        // Arrange
        manufacturer.addFood(new FoodEntity(BEET,"vegetable", 1, 10, 50));


        // Act
        cookBorshch.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookBorshch.getReceipt();
        boolean notWhole = false;
        for(FoodEnum ingredient : receipt){
            if(filledReceipt.get(ingredient) == null) notWhole = true;
        }

        // Assert
        assertTrue(notWhole);
    }

    /**
     * The tries checks the recipe is correct, the ingredients are not missing and everything is ready for cooking.
     */
    @Test
    @Tag("Unittest")
    public void isReadyForCookingTest() {
        // Arrange
        manufacturer.addFood(new FoodEntity(BEET,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(MEAT,"meat", 1, -15, 200));
        manufacturer.addFood(new FoodEntity(POTATO,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(WATER, "water", 1, 20, 0));
        cookBorshch.fillReceipt();

        // Act
        boolean ifReadyForCooking = cookBorshch.isReadyForCooking();

        // Assert
        assertTrue(ifReadyForCooking);

    }

    /**
     * Analyzes the method that should fill missing ingredients in the recipe by creating a recipe without all necessary
     * ingredients for a dish and applying the function.
     */
    @Test
    @Tag("Unittest")
    public void checkIfWeNeedSomethingTest() {
        // Arrange
        manufacturer.addFood(new FoodEntity(MEAT,"meat", 1, -15, 200));
        manufacturer.addFood(new FoodEntity(POTATO,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(WATER, "water", 1, 20, 0));
        cookBorshch.fillReceipt();

        // Act
        cookBorshch.checkIfNeedSomethingForCooking();
        cookBorshch.fillReceipt();

        boolean ifReadyForCooking = cookBorshch.isReadyForCooking();

        // Assert
        assertTrue(ifReadyForCooking);
    }

    /**
     * The tries checks the recipe is correct, the ingredients are not missing and everything is ready for cooking.
     * In this situation there is not enough suitable ingredients for cooking the dish
     */
    @Test
    @Tag("Unittest")
    public void isReadyForCookingWrongTest() {
        // Arrange
        manufacturer.addFood(new FoodEntity(BEET,"vegetable", 1, 10, 50));

        cookBorshch.fillReceipt();

        // Act
        boolean ifReadyForCooking = cookBorshch.isReadyForCooking();

        // Assert
        assertFalse(ifReadyForCooking);
    }

    /**
     * Checks whether the cooking process works as expected, a new food entity is created and it is the right one
     */
    @Test
    @Tag("Unittest")
    public void cookTest() {
        // Arrange
        manufacturer.addFood(new FoodEntity(BEET,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(MEAT,"meat", 1, -15, 200));
        manufacturer.addFood(new FoodEntity(POTATO,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(WATER, "water", 1, 20, 0));

        // Act
        FoodEntity cookedEntity = cookBorshch.cook();


        // Assert
        assertEquals(BORSHCH, cookedEntity.getName());
    }
}