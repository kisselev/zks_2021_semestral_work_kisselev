package strategy_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.strategy.CookBorshch;
import cz.cvut.fel.omo.foodChain.strategy.CookDraniki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.*;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;

class CookDranikiTest {

    TransactionInformer transactionInformer;
    MeatFarmer meatFarmer;
    VegetableFarmer vegetableFarmer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    Manufacturer manufacturer;
    CookDraniki cookDraniki;
    Set<FoodEnum> receipt;

    @BeforeEach
    void prepare(){
        transactionInformer = new TransactionInformer();
        // Create participants
        List<Party> participants = new ArrayList<>();
        manufacturer = new Manufacturer("Test Draniki Manufacturer", 1000, DRANIKI, transactionInformer);

        meatFarmer = new MeatFarmer("Test Meat Farmer", "Test Country", 1000, meatIngredients, transactionInformer);
        vegetableFarmer = new VegetableFarmer("Test Vege Farmer", "Test Country", 2000, vegetableIngredients, transactionInformer);
        Collections.addAll(participants, meatFarmer, vegetableFarmer, manufacturer);

        // Arrange the channels
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        // Register
        manufacturer.registerToTheChannel();
        vegetableFarmer.registerToTheChannel();

        cookDraniki = new CookDraniki(manufacturer);
        receipt = new HashSet<>(Arrays.asList(POTATO, ONION, FLOUR, EGGS));
    }

    /**
     * The test fills the prescribed recipe and cooks a dish from the ingredients
     */
    @Test
    @Tag("Unittest")
    void fillReceiptTest(){
        // Arrange
        manufacturer.addFood(new FoodEntity(POTATO,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(ONION,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(FLOUR,"vegetable", 1, 15, 400));
        manufacturer.addFood(new FoodEntity(EGGS,"meat", 1, 4, 150));

        // Act
        cookDraniki.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookDraniki.getReceipt();

        // Assert
        for(FoodEnum ingredient : receipt){
            assertNotNull(filledReceipt.get(ingredient));
        }
    }

    /**
     * The test fills the prescribed recipe leaving some of the ingredients missing and tries to cook a dish
     */
    @Test
    @Tag("Unittest")
    void fillReceiptNotEnoughItemsTest(){
        // Arrange
        manufacturer.addFood(new FoodEntity(POTATO,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(ONION,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(FLOUR,"vegetable", 1, 15, 400));

        // Act
        cookDraniki.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookDraniki.getReceipt();
        boolean notWhole = false;

        for(FoodEnum ingredient : receipt){
            if(filledReceipt.get(ingredient) == null) notWhole = true;
        }

        // Assert
        assertTrue(notWhole);
    }

    /**
     * The tries checks the recipe is correct, the ingredients are not missing and everything is ready for cooking.
     */
    @Test
    @Tag("Unittest")
    void isReadyForCookingTest() {
        // Arrange
        manufacturer.addFood(new FoodEntity(POTATO,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(ONION,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(FLOUR,"vegetable", 1, 15, 400));
        manufacturer.addFood(new FoodEntity(EGGS,"meat", 1, 4, 150));
        cookDraniki.fillReceipt();

        // Act
        boolean ifReadyForCooking = cookDraniki.isReadyForCooking();

        // Assert
        assertTrue(ifReadyForCooking);

    }

    /**
     * The tries checks the recipe is correct, the ingredients are not missing and everything is ready for cooking.
     * In this situation there is not enough suitable ingredients for cooking the dish
     */
    @Test
    @Tag("Unittest")
    void isReadyForCookingWrongTest() {
        // Arrange
        manufacturer.addFood(new FoodEntity(BEET,"vegetable", 1, 10, 50));
        cookDraniki.fillReceipt();

        // Act
        boolean ifReadyForCooking = cookDraniki.isReadyForCooking();

        // Assert
        assertFalse(ifReadyForCooking);
    }

    /**
     * Analyzes the method that should fill missing ingredients in the recipe by creating a recipe without all necessary
     * ingredients for a dish and applying the function.
     */
    @Test
    @Tag("Unittest")
    public void checkIfWeNeedSomethingTest() {
        // Arrange
        manufacturer.addFood(new FoodEntity(EGGS,"meat", 1, 4, 150));

        // Act
        cookDraniki.checkIfNeedSomethingForCooking();
        cookDraniki.fillReceipt();

        boolean ifReadyForCooking = cookDraniki.isReadyForCooking();

        // Assert
        assertTrue(ifReadyForCooking);
    }

    /**
     * Checks whether the cooking process works as expected, a new food entity is created and it is the right one
     */
    @Test
    @Tag("Unittest")
    void cookTest() {
        // Arrange
        manufacturer.addFood(new FoodEntity(POTATO,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(ONION,"vegetable", 1, 10, 50));
        manufacturer.addFood(new FoodEntity(FLOUR,"vegetable", 1, 15, 400));
        manufacturer.addFood(new FoodEntity(EGGS,"meat", 1, 4, 150));

        // Act
        FoodEntity cookedEntity = cookDraniki.cook();


        // Assert
        assertEquals(DRANIKI, cookedEntity.getName());
    }

}