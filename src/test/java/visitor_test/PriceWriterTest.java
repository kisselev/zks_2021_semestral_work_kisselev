package visitor_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;


import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

import java.nio.channels.SelectableChannel;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import static org.junit.Assert.*;


public class PriceWriterTest {
    private PriceWriter visitor;
    private TransactionInformer transactionInformer;
    private List<FoodEnum>  meatIngredients;
    private List<FoodEnum>  vegetableIngredients;
    private List<FoodEnum>  readyMeals;



    @Before
    public void prepare(){
        visitor = new PriceWriter();
        transactionInformer = new TransactionInformer();
        meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
        vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
        readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    }

    /**
     * Fills the prices for a vegetable farmer and then assures they are correctly set.
     */
    @Test
    @Tag("Unittest")
    public void doForVegetableFarmerTest() {
        VegetableFarmer vegetableFarmer = new VegetableFarmer("Vegetable Farmer Name", "Blank", 1, vegetableIngredients, transactionInformer);
        visitor.doForVegetableFarmer(vegetableFarmer);
        ArrayList<Integer> setPrices = new ArrayList<>();
        for (FoodEnum food_element: vegetableIngredients) {
            setPrices.add(vegetableFarmer.askPrice(food_element, 1));
        }
        List<Integer> list = Arrays.asList(18, 10, 20, 25, 15, 12);
        assertEquals(list, setPrices);
    }

    /**
     * Fills the prices for a meat farmer and then assures they are correctly set.
     */
    @Test
    @Tag("Unittest")
    public void doForMeatFarmerTest() {
        MeatFarmer meatFarmer = new MeatFarmer("Meat Farmer Name", "Meat Country", 1, meatIngredients, transactionInformer);
        visitor.doForMeatFarmer(meatFarmer);
        ArrayList<Integer> setPrices = new ArrayList<>();
        for (FoodEnum food_element: meatIngredients) {
            setPrices.add(meatFarmer.askPrice(food_element, 1));
        }
        List<Integer> list = Arrays.asList(20, 50, 25, 30);
        assertEquals(list, setPrices);
    }

    /**
     * Fills the prices for a manufacturer and then assures they are correctly set.
     */
    @Test
    @Tag("Unittest")
    public void doForManufacturerTest() {
        Manufacturer manufacturer = new Manufacturer("Test Manufacturer Name", 1000, DRANIKI, transactionInformer);
        visitor.doForManufacturer(manufacturer);
        ArrayList<Integer> setPrices = new ArrayList<>();
        for (FoodEnum food_element: readyMeals) {
            setPrices.add(manufacturer.askPrice(food_element, 1));
        }
        List<Integer> list = Arrays.asList(160, 220, 140, 100, 150);
        assertEquals(list, setPrices);
    }

    /**
     * Fills the prices for a storehouse and then assures they are correctly set.
     */
    @Test
    @Tag("Unittest")
    public void doForStorehouseTest() {
        Storehouse storehouse = new Storehouse("Storehouse Name", 10000, transactionInformer);
        visitor.doForStorehouse(storehouse);
        ArrayList<Integer> setPrices = new ArrayList<>();
        List<FoodEnum> allProducts = Stream.concat(vegetableIngredients.stream(), meatIngredients.stream())
                .collect(Collectors.toList());

        allProducts = Stream.concat(allProducts.stream(), readyMeals.stream()).collect(Collectors.toList());
        for (FoodEnum food_element: allProducts) {
            setPrices.add(storehouse.askPrice(food_element, 1));
        }
        List<Integer> list = Arrays.asList(24, 12, 28, 32, 18, 14, 25, 60, 30, 40, 220, 280, 150, 120, 200);
        assertEquals(list, setPrices);
    }

    /**
     * Fills the prices for a seller and then assures they are correctly set.
     */
    @Test
    @Tag("Unittest")
    public void doForSellerTest() {
        Seller seller = new Seller("Test Seller Name", 1000, transactionInformer);
        visitor.doForSeller(seller);
        ArrayList<Integer> setPrices = new ArrayList<>();
        List<FoodEnum> allProducts = Stream.concat(vegetableIngredients.stream(), meatIngredients.stream())
                .collect(Collectors.toList());

        allProducts = Stream.concat(allProducts.stream(), readyMeals.stream()).collect(Collectors.toList());
        for (FoodEnum food_element: allProducts) {
            setPrices.add(seller.askPrice(food_element, 1));
        }
        List<Integer> list = Arrays.asList(30, 20, 34, 42, 25, 18, 35, 68, 40, 50, 240, 300, 170, 140, 220);
        assertEquals(list, setPrices);
    }

    /**
     * Fills the prices for a retailer and then assures they are correctly set.
     */
    @Test
    @Tag("Unittest")
    public void doForRetailerTest() {
        Retailer retailer = new Retailer("Test Retailer Name", 1000, transactionInformer);
        visitor.doForRetailer(retailer);
        ArrayList<Integer> setPrices = new ArrayList<>();
        List<FoodEnum> allProducts = Stream.concat(vegetableIngredients.stream(), meatIngredients.stream())
                .collect(Collectors.toList());

        allProducts = Stream.concat(allProducts.stream(), readyMeals.stream()).collect(Collectors.toList());
        for (FoodEnum food_element: allProducts) {
            setPrices.add(retailer.askPrice(food_element, 1));
        }
        List<Integer> list = Arrays.asList(35, 25, 42, 48, 32, 22, 40, 73, 50, 55, 280, 320, 180, 160, 250);
        assertEquals(list, setPrices);
    }
}
