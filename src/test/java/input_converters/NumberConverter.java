package input_converters;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

import java.util.stream.IntStream;

public class NumberConverter extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
        if (o.equals("None")) {
            return null;
        } else {
            return Integer.valueOf(o.toString());
        }
    }
}
