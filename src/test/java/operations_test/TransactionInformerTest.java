package operations_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.Transaction;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;

import java.io.*;
import java.util.*;

import static cz.cvut.fel.omo.foodChain.Main.logger;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.PANCAKES;


public class TransactionInformerTest {
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<Party> participants;
    TransactionInformer transactionInformer;
    MeatFarmer meatFarmer;
    VegetableFarmer vegetableFarmer;
    Manufacturer manufacturerBorshch;
    Manufacturer manufacturerDraniki;
    Manufacturer manufacturerIceCream;
    Manufacturer manufacturerKyivCutlet;
    Manufacturer manufacturerPancakes;
    Storehouse storehouse;
    Seller seller;
    Retailer retailer;
    Customer customer1;
    Customer customer2;

    @Before
    public void prepare() {
        transactionInformer = new TransactionInformer();
        meatFarmer = new MeatFarmer("Test Meat Farmer", "Test Country", 1000, meatIngredients, transactionInformer);
        vegetableFarmer = new VegetableFarmer("Test Vege Farmer", "Test Country", 2000, vegetableIngredients, transactionInformer);
        manufacturerBorshch = new Manufacturer("Test Manufacturer1", 1000, BORSHCH, transactionInformer);
        manufacturerDraniki = new Manufacturer("Test Manufacturer2", 1000, DRANIKI, transactionInformer);
        manufacturerIceCream = new Manufacturer("Test Manufacturer3", 1000, ICECREAM, transactionInformer);
        manufacturerKyivCutlet = new Manufacturer("Test Manufacturer4", 1000, KYIVCUTLET, transactionInformer);
        manufacturerPancakes = new Manufacturer("Test Manufacturer5", 1000, PANCAKES, transactionInformer);
        storehouse = new Storehouse("Test Store", 1000, transactionInformer);
        seller = new Seller("Test Seller",1000, transactionInformer);
        retailer = new Retailer("Test Retailer",1000, transactionInformer);
        customer1 = new Customer("Test Customer1", 400, transactionInformer);
        customer2 = new Customer("Test Customer2", 1000, transactionInformer);
        participants = new ArrayList<>();
        Collections.addAll(participants,
                meatFarmer,
                vegetableFarmer,
                manufacturerBorshch,
                manufacturerDraniki,
                manufacturerIceCream,
                manufacturerKyivCutlet,
                manufacturerPancakes,
                storehouse,
                seller,
                retailer,
                customer1,
                customer2);
    }


    /**
     * After creating a new transaction, all participants should be notified about the change of the last hash to check,
     * whether the transactions are correct, transparent and safe
     */
    @Test
    @Tag("Unittest")
    public void notifyTest() {
        Request request1 = new Request(customer1, POTATO, 1, vegetableFarmer.getOperationType());
        Transaction new_transaction = new Transaction(request1, vegetableFarmer, customer1.getLastHash());

        for (Party party: participants) {
            transactionInformer.attach(party);
        }

        transactionInformer.notify(new_transaction);

        Assertions.assertTrue(transactionInformer.getParties().stream().allMatch(i -> i.getLastHash() == new_transaction.getHash()));

    }

    /**
     * Simulates the situation when the transaction was added multiple times and created the double spending problem.
     */
    @Test
    @Tag("Unittest")
    public void makeReportTestWhenDoubleHashing() {
        Request request = new Request(customer2, BEET, 1, vegetableFarmer.getOperationType());
        Transaction new_transaction = new Transaction(request, vegetableFarmer, customer2.getLastHash());

        for (Party party: participants) {
            transactionInformer.attach(party);
        }

        transactionInformer.notify(new_transaction);

        // And then notify again

        transactionInformer.notify(new_transaction);

        boolean doubleHashProblem = transactionInformer.makeSecurityReport();

        Assertions.assertTrue(doubleHashProblem);

    }

    /**
     * Creates a report and checks the report has been created correctly and successfully
     */
    @Test
    @Tag("Unittest")
    public void makeReportTest() {
        Request request1 = new Request(customer1, BEET, 1, vegetableFarmer.getOperationType());
        Request request2 = new Request(customer2, MEAT, 1, meatFarmer.getOperationType());

        Transaction new_transaction1 = new Transaction(request1, vegetableFarmer, customer1.getLastHash());

        for (Party party: participants) {
            transactionInformer.attach(party);
        }

        transactionInformer.notify(new_transaction1);

        // And then notify again

        Transaction new_transaction2 = new Transaction(request2, meatFarmer, meatFarmer.getLastHash());

        transactionInformer.notify(new_transaction2);

        boolean doubleHashProblem = transactionInformer.makeSecurityReport();

        Assertions.assertFalse(doubleHashProblem);

    }

    /**
     * Creates a text file report and checks the report has been created correctly and successfully
     */
    @Test
    @Tag("Unittest")
    public void generateTextReportTest() {
        Request request1 = new Request(customer1, BEET, 1, vegetableFarmer.getOperationType());
        Request request2 = new Request(customer2, MEAT, 1, meatFarmer.getOperationType());
        Transaction new_transaction1 = new Transaction(request1, vegetableFarmer, customer1.getLastHash());
        for (Party party: participants) {
            transactionInformer.attach(party);
        }
        transactionInformer.notify(new_transaction1);
        Transaction new_transaction2 = new Transaction(request2, meatFarmer, meatFarmer.getLastHash());
        transactionInformer.notify(new_transaction2);


        String fileName = "TestTransactionReport.txt";

        transactionInformer.generateTextReport(fileName);

        File testFile = new File(fileName);
        boolean exists = testFile.exists();

        Assertions.assertTrue(exists);

        try {
            Scanner scanner = new Scanner(testFile);
            scanner.nextLine();
            Assertions.assertEquals(scanner.nextLine(), new_transaction1.getInfoForText());
            Assertions.assertEquals(scanner.nextLine(), new_transaction2.getInfoForText());
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
