package operations_test;

import cz.cvut.fel.omo.foodChain.chainParticipants.ChainParticipant;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import input_converters.NumberConverter;
import input_converters.StringConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.POTATO;



public class RequestParametrizedTest {
    List<FoodEnum> meatIngredients;
    List<FoodEnum> vegetableIngredients;
    List<FoodEnum> readyMeals;
    List<FoodEnum> coldDessert;
    VegetableFarmer vegetableFarmer;
    TransactionInformer transactionInformer;

    @Before
    public void prepare() {
        transactionInformer = new TransactionInformer();
        vegetableFarmer = new VegetableFarmer("Test Farmer", "Test Country", 2000, vegetableIngredients, transactionInformer);
    }

    /**
     * This test assures the constructor of a Request class is correct, analyzing its parameters, ECs and border values
     * The method for creating combinations is 2 way (pairwise) method.
     * @param applicantName: the name of a customer
     * @param foodName: the object of a request
     * @param quantity: number of entities to create
     * @param expected_outcome: expected outcome of a test
     */
    @ParameterizedTest
    @CsvFileSource(resources = "/Request2way.csv", delimiter = ',', numLinesToSkip = 1)
    @Tag("Parameterized")
    public void requestCreate2wayTest(
            String applicantName,
            String foodName,
            @ConvertWith(NumberConverter.class) Integer quantity,
            @ConvertWith(NumberConverter.class) Integer expected_outcome
    ) {
        int actual_outcome = 1;
        ChainParticipant applicant;
        OperationEnum operationTypeTest;
        if (applicantName.equals("None")) {
            applicant = null;
            operationTypeTest = null;
        }
        else {
            applicant = new VegetableFarmer("Test Farmer", "Test Country", 2000, vegetableIngredients, transactionInformer);
            operationTypeTest = applicant.getOperationType();
        }

        FoodEnum testEnum = BEET;
        if (foodName.equals("None")) testEnum = null;

        try {
            new Request(applicant, testEnum, quantity, operationTypeTest);
        } catch (IllegalArgumentException e) {
            actual_outcome = 0;
        }
        Assertions.assertEquals(expected_outcome, actual_outcome);
    }
}
